Patch for RJW and add-on suite that adds more flavor to text or fills in some missing/placeholder text.

# Currently Patched:

RimJobWorld (rim.job.world)

- Nymph backstories
- Drugs

Licentia Labs (LustLicentia.RJWLabs)

- Stretch serums

## To-Do

RimJobWorld (rim.job.world)

- Hediffs
- Interactions/RulePacks? (check slablover.rjw.sexdescriptions)
- ThingDefs
- Thoughts
- Traits

Chaeren's Apparel
